/* Load the PolymerElement base class and html helper function */
import { PolymerElement, html } from '@polymer/polymer/polymer-element.js';
//import '@polymer/paper-checkbox/paper-checkbox.js';
/* Load shared styles. All view elements use these styles */
import './shared-styles.js';

/* Extend the base PolymerElement class */
class MyNewView extends PolymerElement {
  /* Define a template for the new element */
  static get template() {
    return html`
      <style include="shared-styles">
        :host {
          display: block;

          padding: 10px;
        }
 /*************************************/
 body {font-family: Arial, Helvetica, sans-serif;}

 /* Full-width input fields */
 input[type=text], input[type=password], input[type=email] {
     width: 100%;
     padding: 12px 20px;
     margin: 8px 0;
     display: inline-block;
     border: 1px solid #ccc;
     box-sizing: border-box;
 }

 /* Set a style for all buttons */
 button {
     background-color: #4CAF50;
     color: white;
     padding: 14px 20px;
     margin: 8px 0;
     border: none;
     cursor: pointer;
     width: 100%;
 }

 button:hover {
     opacity: 0.8;
 }

 /* Extra styles for the cancel button */
 .cancelbtn {
     width: auto;
     padding: 10px 18px;
     background-color: #f44336;
 }

 /* Center the image and position the close button */
 .imgcontainer {
     text-align: center;
     margin: 24px 0 12px 0;
     position: relative;
 }

 img.avatar {
     width: 40%;
     border-radius: 50%;
 }

 .container {
     padding: 16px;
 }

 span.psw {
     float: right;
     padding-top: 16px;
 }

 /* The Modal (background) */
 .modal {
     display: none; /* Hidden by default */
     position: fixed; /* Stay in place */
     z-index: 1; /* Sit on top */
     left: 0;
     top: 0;
     width: 100%; /* Full width */
     height: 100%; /* Full height */
     overflow: auto; /* Enable scroll if needed */
     background-color: rgb(0,0,0); /* Fallback color */
     background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
     padding-top: 60px;
 }

 /* Modal Content/Box */
 .modal-content {
     background-color: #fefefe;
     margin: 5% auto 15% auto; /* 5% from the top, 15% from the bottom and centered */
     border: 1px solid #888;
     width: 80%; /* Could be more or less, depending on screen size */
 }

 /* The Close Button (x) */
 .close {
     position: absolute;
     right: 25px;
     top: 0;
     color: #000;
     font-size: 35px;
     font-weight: bold;
 }

 .close:hover,
 .close:focus {
     color: red;
     cursor: pointer;
 }

 /* Add Zoom Animation */
 .animate {
     -webkit-animation: animatezoom 0.6s;
     animation: animatezoom 0.6s
 }

 @-webkit-keyframes animatezoom {
     from {-webkit-transform: scale(0)}
     to {-webkit-transform: scale(1)}
 }

 @keyframes animatezoom {
     from {transform: scale(0)}
     to {transform: scale(1)}
 }

 /* Change styles for span and cancel button on extra small screens */
 @media screen and (max-width: 300px) {
     span.psw {
        display: block;
        float: none;
     }
     .cancelbtn {
        width: 100%;
     }
 }
 /*************************************/
      </style>

      <div class="card">
        <!-- <div class="circle">L</div> -->

     <div class="imgcontainer">
       <img width="70px" height="70px" src="https://image.flaticon.com/icons/svg/1092/1092848.svg" alt="Avatar" class="avatar">
     </div>

     <div class="container">

       <!--<form name="login" method="POST" onSubmit="return myFunction()" action="http://localhost:3000/api/Users/login">-->
       <!--<form name="login" onSubmit="return myFunction()">-->
       <!--<form name="login" id="form" method="get" onSubmit="return myFunction()" action="success.htm">-->
       <form name="form" id="form">
         <div class="field">
           <label for="username"><b>Usuario</b></label>
           <input type="text" id="username" placeholder="Usuario" name="username" required></code>
         </div>

         <div class="field">
           <label for="email"><b>Email</b></label>
           <input type="email" id="email" placeholder="Email" name="email" required></code>
         </div>

         <div class="field">
           <label for="password"><b>Password</b></label>
           <!-- <input type="password" id="password" placeholder="Password" name="password" required pattern="(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$"></code> -->
           <input type="password" id="password" placeholder="Teclee su password" name="password" required pattern="(.{8,})" title="Su password debe ser mayor o igual a 8 caracteres y contener al menos un numero, una letra mayuscula y minuscula">
         </div>
         <button onclick="myFunction()">Login</button>
         <button type="button" class="cancelbtn">Logout</button>
         <span class="psw">Olvido su <a href="#">password?</a></span>
       </div>
     </form>
     <script>
       function myFunction() {
         var url="http://localhost:3000/api/Users/login";
         var request=new XMLHttpRequest();

         request.onreadystatechange=function(){
			 if (this.readyState == 4 && this.status == 200){
				 console.log(JSON.parse(request.status));
				 var respuesta_id = JSON.parse(request.responseText).id;
				 console.log(respuesta_id);
				 document.cookie = "tokenID=" + respuesta_id;
				 window.open ('success.htm', '_blank');
		     }else{
				 if (this.readyState == 4){
					 document.cookie = "tokenID=";
				     console.log("Error en Login, favor de validar... Codigo de error: " + this.status + ".");
                     window.open ('unsuccess.htm', '_blank');
			     }
		     }
	     }
         request.open("POST", url, false);
         request.setRequestHeader("Accept", "application/json");
         request.setRequestHeader("Content-Type", "application/json");
         var login={username: form.username.value,
                    email: form.email.value,
                    password: form.password.value};
         /*var login={"username": "user_01",
                    "email": "user_01@dominio.com",
                    "password": "passwd_001"};*/

         var body = JSON.stringify(login);
         request.send(body);
       }
     </script>
    `;
  }}

/* Register the new element with the browser */
window.customElements.define('my-new-view', MyNewView);