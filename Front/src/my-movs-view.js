/* Load the PolymerElement base class and html helper function */
import { PolymerElement, html } from '@polymer/polymer/polymer-element.js';
/* Load shared styles. All view elements use these styles */
import './shared-styles.js';
import '@vaadin/vaadin-grid/vaadin-grid.js';


/* Extend the base PolymerElement class */
class MyMovsView extends PolymerElement {
  /* Define a template for the new element */
  static get template() {
    return html`
      <style include="shared-styles">
        :host {
          display: block;

          padding: 10px;
        }
        td, th {
          padding: 8px;
          box-sizing: border-box;
          white-space: nowrap;
        }
        td:nth-of-type(1),
        th:nth-of-type(1) {
          background-color: rgba(255, 0, 255, 0.2);
        }
        tr.iron-selected td {
          background-color: rgba(0, 0, 0, 0.1);
        }
        tr:hover td {
          background-color: rgba(0, 0, 0, 0.2);
        }
        tr td.iron-selected:not(:nth-of-type(1)) {
          background-color: rgba(255, 255, 0, 0.2);
        }
      </style>

      <div class="card">
        <div class="circle">MV</div>
        <h1>Consulta de movimientos</h1>
        <p>Consulta los movimientos que se encuentran en la BD. Se requiere que previamente se haya hecho Login.</p>
        <br>
        <h1>Movimientos</h1>
        <table is="s-table" id="s-table-lite" name="s-table-lite" fixed-column>
          <thead>
            <tr>
              <th>ID Ciente</th>
			  <th>Nombre</th>
			  <!--<th>user_email</th>-->
			  <th>Fecha del movimiento</th>
			  <th>Importe</th>
			  <th>Categoria</th>
            </tr>
          </thead>
          <tbody is="s-tbody" id="s-tbody" name="s-tbody">
          </tbody>
        </table>
      </div>
      <script>
       var tabla = document.getElementsByName("s-table-lite")[0];
       var tblBody = document.getElementsByName("s-tbody")[0];

       function displayTable(datos) {
		   for(var i=0;i < datos.length;i++){
			   var hilera = document.createElement("tr");
			   var celda = document.createElement("td"); celda.appendChild(document.createTextNode(datos[i].idcliente)); hilera.appendChild(celda);
			   var celda = document.createElement("td"); celda.appendChild(document.createTextNode(datos[i].nombre + " " + datos[i].apellido)); hilera.appendChild(celda);
			   //var celda = document.createElement("td"); celda.appendChild(document.createTextNode(datos[i].email)); hilera.appendChild(celda);
			   var celda = document.createElement("td"); celda.appendChild(document.createTextNode(datos[i].fecha)); hilera.appendChild(celda);
			   var celda = document.createElement("td"); celda.appendChild(document.createTextNode(datos[i].importe)); hilera.appendChild(celda);
			   var celda = document.createElement("td"); celda.appendChild(document.createTextNode(datos[i].categoria)); hilera.appendChild(celda);
		       console.log(datos[i]);
               tblBody.appendChild(hilera);
		   }
		   tabla.appendChild(tblBody);
	   }

       function viewMovimientos() {
         var url="http://localhost:3000/api/movimientos";
         var request=new XMLHttpRequest();

         request.onreadystatechange=function(){
			 if (this.readyState == 4 && this.status == 200){
				 console.log(JSON.parse(request.responseText));
                 displayTable(JSON.parse(request.responseText));
		     }else{
				 if (this.readyState == 4){
				     console.log("Error al cargar datos de usuarios...");
			     }
		     }
	     }
         request.open("GET", url, true);
         request.setRequestHeader("Accept", "application/json");
         request.setRequestHeader("Content-Type", "application/json");
         request.send();
       }

       viewMovimientos();


      </script>
    `;
  }
}
/* Register the new element with the browser */
window.customElements.define('my-movs-view', MyMovsView);