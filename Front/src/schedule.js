//Arreglo de conferencias
var schedule = [
    {
      "id":"session-1",
      "title":"Registration",
      "tracks":[1,2]
    },
    {
      "id":"session-2",
      "title":"HTML5 Basic",
      "tracks":[1,2]
    },
    {
       "id":"session-3",
       "title":"HTML5 and WebSockets",
       "tracks":[1]
     },
     {
       "id":"session-4",
       "title":"HTML5 in enterprise applications",
       "tracks":[2]
     },
     {
       "id":"session-5",
       "title":"Node.js",
       "tracks":[1]
     },
     {
       "id":"session-6",
       "title":"Coffee Break",
       "tracks":[1,2]
     },
     {
       "id":"session-7",
       "title":"Responsive design",
       "tracks":[1]
     }

];

//Crear un par de listas de conferencias (Track 1 y Track 2)
//Estás se desplegarán conforme se activen los checkbox Track1 y Track2

//Creamos una variable que haga referencia a "schedule"
var lista = document.getElementById("schedule");

//Variables que hagan referencia a los Checkbox
var Track1CheckBox = document.getElementById("show-track-1");
var Track2CheckBox = document.getElementById("show-track-2");

//Función para que limpie la lista de elementos
function clearList(){
    while(lista.firstChild){
        lista.removeChild(lista.firstChild);
    }
}

//Función que llene la lista conforme los checkbox


function createSessionElement(session){
    var li = document.createElement("li");
    li.textContent = session.title;
    return li;
}

//Función que despliegue las conferencias
function displaySchedule(){ 
    clearList();
    //Recorremos la lista de conferencias
    for (var i=0; i<schedule.length; i++){
        var tracks = schedule[i].tracks;
        if ((Track1CheckBox.checked && tracks.indexOf(1) >= 0) ||
            (Track2CheckBox.checked && tracks.indexOf(2) >= 0)){
                var li = createSessionElement(schedule[i]);
                lista.appendChild(li);
            } 
        tracks.includes("2");
    }
}

displaySchedule();
//Asociar eventos a los checkbox
Track1CheckBox.addEventListener("click", displaySchedule, false);
Track2CheckBox.addEventListener("click", displaySchedule, false);