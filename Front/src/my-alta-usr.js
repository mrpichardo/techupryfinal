/* Load the PolymerElement base class and html helper function */
import { PolymerElement, html } from '@polymer/polymer/polymer-element.js';
/* Load shared styles. All view elements use these styles */
import './shared-styles.js';

/* Extend the base PolymerElement class */
class MyAltaUsr extends PolymerElement {
  /* Define a template for the new element */
  static get template() {
    return html`
      <style include="shared-styles">
        :host {
          display: block;

          padding: 10px;
        }
        body {font-family: Arial, Helvetica, sans-serif;}

        /* Full-width input fields */
        input[type=text],     input[type=password],
        input[type=email],    input[type=date],
        input[type=currency], input[type=number] {
          width: 100%;
          padding: 12px 20px;
          margin: 8px 0;
          display: inline-block;
          border: 1px solid #ccc;
          box-sizing: border-box;
        }

        /* Set a style for all buttons */
        button {
          background-color: #4CAF50;
          color: white;
          padding: 14px 20px;
          margin: 8px 0;
          border: none;
          cursor: pointer;
          width: 100%;
        }

        button:hover {
          opacity: 0.8;
        }

        /* Extra styles for the cancel button */
        .cancelbtn {
          width: auto;
          padding: 10px 18px;
          background-color: #f44336;
        }

        /* Center the image and position the close button */
        .imgcontainer {
           text-align: center;
           margin: 24px 0 12px 0;
           position: relative;
        }

        img.avatar {
          width: 40%;
          border-radius: 50%;
        }

        .container {
          padding: 16px;
        }

        span.psw {
          float: right;
          padding-top: 16px;
        }

        /* The Close Button (x) */
        .close {
           position: absolute;
           right: 25px;
           top: 0;
           color: #000;
           font-size: 35px;
           font-weight: bold;
        }

        .close:hover,
        .close:focus {
           color: red;
           cursor: pointer;
        }

        /* Add Zoom Animation */
        .animate {
          -webkit-animation: animatezoom 0.6s;
          animation: animatezoom 0.6s
        }

        @-webkit-keyframes animatezoom {
          from {-webkit-transform: scale(0)}
            to {-webkit-transform: scale(1)}
        }

        @keyframes animatezoom {
          from {transform: scale(0)}
            to {transform: scale(1)}
        }

        /* Change styles for span and cancel button on extra small screens */
        @media screen and (max-width: 300px) {
           span.psw {
             display: block;
             float: none;
           }
           .cancelbtn {
             width: 100%;
           }
        }
      </style>

      <div class="card">
        <div class="circle">AU</div>
        <h1>Alta de Usuarios</h1>
        <p>Da de alta un usuario, nivel aplicacion, en la BD.</p>
        <div class="container">
           <form name="form" id="form">
              <div class="field">
                 <label for="user_id"><b>User ID</b></label>
                 <input type="text" id="user_id" placeholder="User ID" name="user_id" required></code>
              </div>
              <div class="field">
                 <label for="user_name"><b>Nombre de usuario</b></label>
                 <input type="text" id="user_name" placeholder="Nombre de usuario" name="user_name" required></code>
              </div>
              <div class="field">
                 <label for="user_email"><b>Email</b></label>
                 <input type="email" id="user_email" placeholder="Email" name="user_email" required></code>
              </div>
              <div class="field">
                 <label for="password"><b>Password</b></label>
                 <input type="password" id="password" placeholder="Teclee su password" name="password" required pattern="(.{8,})" title="Su password debe ser mayor o igual a 8 caracteres y contener al menos un numero, una letra mayuscula y minuscula"></code>
              </div>
              <div class="field">
                 <label for="confirmPassword"><b>Confirme Password</b></label>
                 <input type="password" id="confirmPassword" placeholder="Confirme su password" name="confirmPassword" required pattern="(.{8,})" title="Su password debe ser mayor o igual a 8 caracteres y contener al menos un numero, una letra mayuscula y minuscula"></code>
              </div>
              <button onclick="userRegister()">Dar de alta</button>
              <button type="button" class="cancelbtn">Cancelar</button>
              <span class="psw"><a href="#">Inicio</a></span>
              <br><br>
           </form>
        </div>
      </div>
      <script>

      var tokenID="";

      function myToken(){
         var name = "tokenID=";
         var x = document.cookie;
         var decodedCookie = decodeURIComponent(document.cookie);
         var ca = decodedCookie.split(';');

         for(var i = 0; i <ca.length; i++) {
             var c = ca[i];
             while (c.charAt(0) == ' ') {
                    c = c.substring(1);
             }
             if (c.indexOf(name) == 0) {
                tokenID = c.substring(name.length, c.length);
                //url=url + "?access_token=" + tokenID;
             }
	     }
		 return tokenID;
      }

      function myCurrentDate(){
	      var d = new Date();
	      var y = d.getFullYear();
	      var m = d.getMonth() + 1;
	      var d = d.getDate();
	      var fecha = m + "/" + d + "/" + y;
	      return fecha;
      }

      function altaUsr(){
         var url="http://localhost:3000/api/system_users" + "?access_token=" + myToken();
         var request=new XMLHttpRequest();
         request.onreadystatechange=function(){
			 console.log(this.readyState + " " + this.status);
			 if (this.readyState == 4 && this.status == 200){
				 console.log("Alta de usuario realizada!!!");
				 window.open ('success.htm', '_blank');
		     }else{
				 if (this.readyState == 4){
					 alert("Error al dar de alta el usuario, favor de validar... Codigo de error: " + this.status + ".");
				     console.log("Error al dar de alta el usuario, favor de validar... Codigo de error: " + this.status + ".");
                     window.open ('unsuccess.htm', '_blank');
			     }
		     }
	     }
	     console.log(url);
         request.open("POST", url, false);
         request.setRequestHeader("Accept", "application/json");
         request.setRequestHeader("Content-Type", "application/json");
         var status = "A";
         var intentos = 0;
         var fecha = myCurrentDate();
         var movimiento={user_id: form.user_id.value,
                         password: form.password.value,
                         user_name: form.user_name.value,
                         user_status: status,
                         user_fec_mod: fecha,
                         user_email: form.user_email.value,
                         user_fec_pass: fecha,
                         user_intentos: intentos};
         var body = JSON.stringify(movimiento);
         console.log(body);
         request.send(body);
      }

      function userRegister(){
		var passwordMatch = form.password.value == form.confirmPassword.value;
		if(passwordMatch){
		   console.log("Confirmacion OK!!! Procesando alta...");
		   altaUsr();
		} else {
		   console.log("El Password y su confirmacion no coinciden. Favor de validar.");
		   alert("El Password y su confirmacion no coinciden. Favor de validar.");
		}
      }
      </script>
    `;
  }
}
/* Register the new element with the browser */
window.customElements.define('my-alta-usr', MyAltaUsr);