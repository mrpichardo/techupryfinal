/**
 * @license
 * Copyright (c) 2016 The Polymer Project Authors. All rights reserved.
 * This code may only be used under the BSD style license found at http://polymer.github.io/LICENSE.txt
 * The complete set of authors may be found at http://polymer.github.io/AUTHORS.txt
 * The complete set of contributors may be found at http://polymer.github.io/CONTRIBUTORS.txt
 * Code distributed by Google as part of the polymer project is also
 * subject to an additional IP rights grant found at http://polymer.github.io/PATENTS.txt
 */

import { PolymerElement, html } from '@polymer/polymer/polymer-element.js';
import './shared-styles.js';
import '@vaadin/vaadin-grid/vaadin-grid.js';


class MyView3 extends PolymerElement {
  static get template() {
    return html`

      <style include="shared-styles">
        :host {
          display: block;

          padding: 10px;
        }

        td, th {
          padding: 8px;
          box-sizing: border-box;
          white-space: nowrap;
        }
        td:nth-of-type(1),
        th:nth-of-type(1) {
          background-color: rgba(255, 0, 255, 0.2);
        }
        tr.iron-selected td {
          background-color: rgba(0, 0, 0, 0.1);
        }
        tr:hover td {
          background-color: rgba(0, 0, 0, 0.2);
        }
        tr td.iron-selected:not(:nth-of-type(1)) {
          background-color: rgba(255, 255, 0, 0.2);
        }
      </style>

      <div class="card">
        <div class="circle">CU</div>
        <h1>Consulta de usuarios.</h1>
        <p>Realiza la consulta de la tabla de usuarios. Se trae los datos siempre y cuando se haya hecho login antes.</p>
        <p>Se presenta un ejemplo de una lista y tabla dinamicas.</p>
        <p>El trazado de la tabla HTML es mediante JavaScript y la Interface DOM.</p>
        <p>Para la tabla se utiliza un WebComponent s-table de s-elements.</p>
        <br><br>
        <h1>Lista</h1>
        <div class="ul"></div>
        <br><br>
        <h1>Tabla</h1>
        <table is="s-table" id="s-table-lite" name="s-table-lite" fixed-column>
          <thead>
            <tr>
              <th>id</th>
			  <th>user_id</th>
			  <th>user_name</th>
			  <th>user_email</th>
			  <th>Status</th>
			  <th>Intentos</th>
            </tr>
          </thead>
          <tbody is="s-tbody" id="s-tbody" name="s-tbody">
            <!--<tr is="s-tr" multi>
              <td>5baa96710c0ef14c3018e4f1</td>
			  <td>user_01</td>
			  <td>UserName_01</td>
			  <td>user_01@dominio.com</td>
			  <td>A</td>
			  <td>0</td>
            </tr>-->
          </tbody>
        </table>
        <br>
        <!--<button onclick="myTable()">Try it</button><br></br>
        <input type="button" value="Genera una tabla" onclick="genera_tabla()">-->
      </div>

     <script>
       var lista = document.getElementsByClassName("ul")[0];
       var tabla = document.getElementsByName("s-table-lite")[0];
       var tblBody = document.getElementsByName("s-tbody")[0];

       // Funcion limpiar lista
       function clearTable(){
		   console.log("clearTable()...");
           while(lista.firstChild){
			     tblBody.firstChild(hilera);
                 tblBody.removeChild(lista.firstChild);
           }
       }

       function displayTable(datos) {
		   //clearTable();
		   for(var i=0;i < datos.length;i++){
			   var hilera = document.createElement("tr");
			   var celda = document.createElement("td"); celda.appendChild(document.createTextNode(datos[i].id)); hilera.appendChild(celda);
			   var celda = document.createElement("td"); celda.appendChild(document.createTextNode(datos[i].user_id)); hilera.appendChild(celda);
			   var celda = document.createElement("td"); celda.appendChild(document.createTextNode(datos[i].user_name)); hilera.appendChild(celda);
			   var celda = document.createElement("td"); celda.appendChild(document.createTextNode(datos[i].user_email)); hilera.appendChild(celda);
			   var celda = document.createElement("td"); celda.appendChild(document.createTextNode(datos[i].user_status)); hilera.appendChild(celda);
			   var celda = document.createElement("td"); celda.appendChild(document.createTextNode(datos[i].user_intentos)); hilera.appendChild(celda);
		       console.log(datos[i]);
               tblBody.appendChild(hilera);
		   }
		   tabla.appendChild(tblBody);
	   }


       function myTable() {
           // Crea las celdas
           for (var i = 0; i < 2; i++) {
             // Crea las hileras de la tabla
             var hilera = document.createElement("tr");
             for (var j = 0; j < 2; j++) {
               // Crea un elemento <td> y un nodo de texto, haz que el nodo de
               // texto sea el contenido de <td>, ubica el elemento <td> al final
               // de la hilera de la tabla
               var celda = document.createElement("td");
               var textoCelda = document.createTextNode("celda en la hilera "+i+", columna "+j);
               celda.appendChild(textoCelda);
               hilera.appendChild(celda);
             }
             // agrega la hilera al final de la tabla (al final del elemento tblbody)
             tblBody.appendChild(hilera);
	       }
           // posiciona el <tbody> debajo del elemento <table>
           tabla.appendChild(tblBody);
           // appends <table> into <body>
           body.appendChild(tabla);
	   }

       // Funcion limpiar lista
       function clearList(){
		   console.log(lista);
           while(lista.firstChild){
                 lista.removeChild(lista.firstChild);
           }
       }

       // Funcion generar elemento de la lista
	   function createSessionElement_01(session){
	       var li = document.createElement("li");
	       li.textContent = session.user_email;
	       return li;
	   }

       // Funcion desplegar los datos
       function displaySchedule_01(datos){
           clearList();
           for(var i=0;i < datos.length;i++){
			   console.log(datos[i]);
               var li = createSessionElement_01(datos[i]);
               lista.appendChild(li);
           }
       }

       function viewThreeFunction() {
         var url="http://localhost:3000/api/system_users";
         var request=new XMLHttpRequest();
         var name = "tokenID=";
         var tokenID="";
         var x = document.cookie;
         var decodedCookie = decodeURIComponent(document.cookie);
         var ca = decodedCookie.split(';');

         //alert("Pausa");
         for(var i = 0; i <ca.length; i++) {
             var c = ca[i];
             while (c.charAt(0) == ' ') {
                    c = c.substring(1);
             }
             if (c.indexOf(name) == 0) {
                tokenID = c.substring(name.length, c.length);
                url=url + "?access_token=" + tokenID;
             }
	     }

         request.onreadystatechange=function(){
			 if (this.readyState == 4 && this.status == 200){
				 console.log(JSON.parse(request.responseText));
                 displaySchedule_01(JSON.parse(request.responseText));
                 displayTable(JSON.parse(request.responseText));
		     }else{
				 if (this.readyState == 4){
				     console.log("Error al cargar datos de usuarios...");
			     }
		     }
	     }
         request.open("GET", url, true);
         request.setRequestHeader("Accept", "application/json");
         request.setRequestHeader("Content-Type", "application/json");
         request.send();
       }

       //Ejecuta el script
       //displaySchedule();
       viewThreeFunction();
     </script>

    `;
  }
}

window.customElements.define('my-view3', MyView3);
//window.onload = viewThreeFunction();
