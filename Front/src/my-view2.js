/**
 * @license
 * Copyright (c) 2016 The Polymer Project Authors. All rights reserved.
 * This code may only be used under the BSD style license found at http://polymer.github.io/LICENSE.txt
 * The complete set of authors may be found at http://polymer.github.io/AUTHORS.txt
 * The complete set of contributors may be found at http://polymer.github.io/CONTRIBUTORS.txt
 * Code distributed by Google as part of the polymer project is also
 * subject to an additional IP rights grant found at http://polymer.github.io/PATENTS.txt
 */

import { PolymerElement, html } from '@polymer/polymer/polymer-element.js';
import './shared-styles.js';
import '@vaadin/vaadin-grid/vaadin-grid.js';

class MyView2 extends PolymerElement {
  static get template() {
    return html`

      <style>
        :host {
          display: block;

          padding: 10px;
        }

        td, th {
          padding: 8px;
          box-sizing: border-box;
          white-space: nowrap;
        }
        td:nth-of-type(1),
        th:nth-of-type(1) {
          background-color: rgba(255, 0, 255, 0.2);
        }
        tr.iron-selected td {
          background-color: rgba(0, 0, 0, 0.1);
        }
        tr:hover td {
          background-color: rgba(0, 0, 0, 0.2);
        }
        tr td.iron-selected:not(:nth-of-type(1)) {
          background-color: rgba(255, 255, 0, 0.2);
        }
      </style>

      <div class="card">
        <div class="circle">API</div>
        <h1>Mostrar datos de otras APIs</h1>
        <p>1. Muestra los datos geograficos utilizando la API de Google.</p>
        <div id="localizacion" name="localizacion">
        </div>
        <!--<div id="googleMap" name="googleMap" style="width:100%;height:400px;"></div>-->

        <h2> API: https://jsonplaceholder.typicode.com/users </h2>
        <table is="s-table-lite" id="s-table-lite" name="s-table-lite" fixed-column>
          <thead>
            <tr>
              <th>ID</th>
			  <th>Clave</th>
			  <th>Nombre</th>
			  <th>Correo electronico</th>
			  <th>Ciudad</th>
			  <th>Codigo Postal</th>
			  <th>Telefono</th>
			  <th>Empresa</th>
            </tr>
          </thead>
          <tbody is="s-tbody" id="s-tbody" name="s-tbody">
          </tbody>
        </table>

        <h2> API: https://services.odata.org/V4/Northwind/Northwind.svc/Customers </h2>
        <table is="s-table-lite" id="s-table-lite" name="s-table-lite" fixed-column>
          <thead>
            <tr>
              <th>ID</th>
			  <th>Empresa</th>
			  <th>Contacto</th>
			  <th>Departamento</th>
			  <th>Ciudad</th>
			  <th>Direccion</th>
			  <th>Telefono</th>
			  <th>Codigo Postal</th>
            </tr>
          </thead>
          <tbody is="s-tbody" id="s-tbody" name="s-tbody">
          </tbody>
        </table>

      </div>
      <script>
       var tabla = document.getElementsByName("s-table-lite")[0];
       var tblBody = document.getElementsByName("s-tbody")[0];


        function myMap(posicion){
		  var mapProp={center:new google.maps.LatLng( posicion.coords.latitude, posicion.coords.longitude), zoom:50,};
		  var map=new google.maps.Map(document.getElementsByName("googleMap")[0],mapProp);
		}

		function mostrar(posicion){
		  var ubicacion=document.getElementsByName("localizacion")[0];
		  var datos='';
		  datos+='Latitud: '+posicion.coords.latitude+'<br>';
		  datos+='Longitud: '+posicion.coords.longitude+'<br>';
		  datos+='Exactitud: '+posicion.coords.accuracy+' metros.<br>';
		  ubicacion.innerHTML=datos;
		  myMap(posicion);
        }

        function gestionarErrores(error){
		  alert("Error: " + error.code + " " + error.message + ".");
		}

        function obtener(){
		  navigator.geolocation.getCurrentPosition(mostrar, gestionarErrores);
	    }

       function displayTable(datos, id) {
		   var ln = 0;
		   if (id == 0){
               var tabla = document.getElementsByName("s-table-lite")[0];
               var tblBody = document.getElementsByName("s-tbody")[0];
               ln = datos.length;
               console.log(datos);
	       }
		   if (id == 1){
               var tabla = document.getElementsByName("s-table-lite")[1];
               var tblBody = document.getElementsByName("s-tbody")[1];
               ln = datos.value.length;
               console.log(datos.value);
	       }
		   for(var i=0;i < ln;i++){
			   if (id == 0){
                   var campo_01 = datos[i].id;
                   var campo_02 = datos[i].username;
                   var campo_03 = datos[i].name;
                   var campo_04 = datos[i].email;
                   var campo_05 = datos[i].address.city;
                   var campo_06 = datos[i].address.zipcode;
                   var campo_07 = datos[i].phone;
				   var campo_08 = datos[i].company.name;
		       }
			   if (id == 1){
                   var campo_01 = datos.value[i].CustomerID;
                   var campo_02 = datos.value[i].CompanyName;
                   var campo_03 = datos.value[i].ContactName;
                   var campo_04 = datos.value[i].ContactTitle;
                   var campo_05 = datos.value[i].City;
                   var campo_06 = datos.value[i].Address;
                   var campo_07 = datos.value[i].Phone;
				   var campo_08 = datos.value[i].PostalCode;
		       }
			   var hilera = document.createElement("tr");
			   var celda = document.createElement("td"); celda.appendChild(document.createTextNode(campo_01)); hilera.appendChild(celda);
			   var celda = document.createElement("td"); celda.appendChild(document.createTextNode(campo_02)); hilera.appendChild(celda);
			   var celda = document.createElement("td"); celda.appendChild(document.createTextNode(campo_03)); hilera.appendChild(celda);
			   var celda = document.createElement("td"); celda.appendChild(document.createTextNode(campo_04)); hilera.appendChild(celda);
			   var celda = document.createElement("td"); celda.appendChild(document.createTextNode(campo_05)); hilera.appendChild(celda);
			   var celda = document.createElement("td"); celda.appendChild(document.createTextNode(campo_06)); hilera.appendChild(celda);
			   var celda = document.createElement("td"); celda.appendChild(document.createTextNode(campo_07)); hilera.appendChild(celda);
			   var celda = document.createElement("td"); celda.appendChild(document.createTextNode(campo_08)); hilera.appendChild(celda);
               tblBody.appendChild(hilera);
		   }
		   tabla.appendChild(tblBody);
	   }

       function cargaDatosAPI(url, id) {
         //var url="http://localhost:3000/api/system_users";
         var request=new XMLHttpRequest();

         request.onreadystatechange=function(){
			 if (this.readyState == 4 && this.status == 200){
				 //console.log(JSON.parse(request.responseText));
                 displayTable(JSON.parse(request.responseText), id);
		     }else{
				 if (this.readyState == 4){
				     console.log("Error al cargar datos de la API: " + url + "...");
			     }
		     }
	     }
         request.open("GET", url, false);
         //request.setRequestHeader("Accept", "application/json");
         //request.setRequestHeader("Content-Type", "application/json");
         request.send();
       }

		obtener();
		cargaDatosAPI("https://jsonplaceholder.typicode.com/users", 0);
		cargaDatosAPI("https://services.odata.org/V4/Northwind/Northwind.svc/Customers", 1);
      </script>
      <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBu-916DdpKAjTmJNIgngS6HL_kDIKU0aU&callback=myMap"></script>
    `;
  }
}

window.customElements.define('my-view2', MyView2);
