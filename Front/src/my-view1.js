/**
 * @license
 * Copyright (c) 2016 The Polymer Project Authors. All rights reserved.
 * This code may only be used under the BSD style license found at http://polymer.github.io/LICENSE.txt
 * The complete set of authors may be found at http://polymer.github.io/AUTHORS.txt
 * The complete set of contributors may be found at http://polymer.github.io/CONTRIBUTORS.txt
 * Code distributed by Google as part of the polymer project is also
 * subject to an additional IP rights grant found at http://polymer.github.io/PATENTS.txt
 */

import { PolymerElement, html } from '@polymer/polymer/polymer-element.js';
import './shared-styles.js';

class MyView1 extends PolymerElement {
  static get template() {
    return html`
      <style include="shared-styles">
        :host {
          display: block;

          padding: 10px;
        }
        body {font-family: Arial, Helvetica, sans-serif;}

        /* Full-width input fields */
        input[type=text], input[type=password], input[type=email] {
          width: 100%;
          padding: 12px 20px;
          margin: 8px 0;
          display: inline-block;
          border: 1px solid #ccc;
          box-sizing: border-box;
        }

        /* Set a style for all buttons */
        button {
          background-color: #4CAF50;
          color: white;
          padding: 14px 20px;
          margin: 8px 0;
          border: none;
          cursor: pointer;
          width: 100%;
        }

        button:hover {
          opacity: 0.8;
        }

        /* Extra styles for the cancel button */
        .cancelbtn {
          width: auto;
          padding: 10px 18px;
          background-color: #f44336;
        }

        /* Center the image and position the close button */
        .imgcontainer {
           text-align: center;
           margin: 24px 0 12px 0;
           position: relative;
        }

        img.avatar {
          width: 40%;
          border-radius: 50%;
        }

        .container {
          padding: 16px;
        }

        span.psw {
          float: right;
          padding-top: 16px;
        }

        /* The Close Button (x) */
        .close {
           position: absolute;
           right: 25px;
           top: 0;
           color: #000;
           font-size: 35px;
           font-weight: bold;
        }

        .close:hover,
        .close:focus {
           color: red;
           cursor: pointer;
        }

        /* Add Zoom Animation */
        .animate {
          -webkit-animation: animatezoom 0.6s;
          animation: animatezoom 0.6s
        }

        @-webkit-keyframes animatezoom {
          from {-webkit-transform: scale(0)}
            to {-webkit-transform: scale(1)}
        }

        @keyframes animatezoom {
          from {transform: scale(0)}
            to {transform: scale(1)}
        }

        /* Change styles for span and cancel button on extra small screens */
        @media screen and (max-width: 300px) {
           span.psw {
             display: block;
             float: none;
           }
           .cancelbtn {
             width: 100%;
           }
        }
      </style>

      <div class="card">
        <div class="circle">RU</div>
        <h1>Registro</h1>
        <p>Registro de usuarios, en al parte de la BD de seguridad.</p>
        <p></p>
        <div class="container">
           <form name="form" id="form">
              <div class="field">
                 <label for="username"><b>Usuario</b></label>
                 <input type="text" id="username" placeholder="Usuario" name="username" required></code>
              </div>
              <div class="field">
                 <label for="email"><b>Email</b></label>
                 <input type="email" id="email" placeholder="Email" name="email" required></code>
              </div>
              <div class="field">
                 <label for="password"><b>Password</b></label>
                 <input type="password" id="password" placeholder="Teclee su password" name="password" required pattern="(.{8,})" title="Su password debe ser mayor o igual a 8 caracteres y contener al menos un numero, una letra mayuscula y minuscula">
              </div>
              <div class="field">
                 <label for="confirmPassword"><b>Confirme Password</b></label>
                 <input type="password" id="confirmPassword" placeholder="Confirme su password" name="confirmPassword" required pattern="(.{8,})" title="Su password debe ser mayor o igual a 8 caracteres y contener al menos un numero, una letra mayuscula y minuscula">
              </div>
              <button onclick="userRegister()">Registrar</button>
              <button type="button" class="cancelbtn">Cancelar</button>
              <span class="psw"><a href="#">Inicio</a></span>
              <br><br>
           </form>
        </div>
      </div>
      <script>

      function altaRegister(){
         var url="http://localhost:3000/api/Users";
         var request=new XMLHttpRequest();
         request.onreadystatechange=function(){
			 if (this.readyState == 4 && this.status == 200){
				 console.log(JSON.parse(request.status));
				 window.open ('success.htm', '_blank');
		     }else{
				 if (this.readyState == 4){
					 alert("Error en alta de login, favor de validar... Codigo de error: " + this.status + ".");
				     console.log("Error en alta de login, favor de validar... Codigo de error: " + this.status + ".");
                     window.open ('unsuccess.htm', '_blank');
			     }
		     }
	     }
         request.open("POST", url, false);
         request.setRequestHeader("Accept", "application/json");
         request.setRequestHeader("Content-Type", "application/json");
         var login={username: form.username.value,
                    email: form.email.value,
                    password: form.password.value};
         var body = JSON.stringify(login);
         request.send(body);
      }

      function userRegister(){
		var passwordMatch = form.password.value == form.confirmPassword.value;
		if(passwordMatch){
		   console.log("Confirmacion OK!!! Procesando alta...");
		   altaRegister();
		} else {
		   console.log("El Password y su confirmacion no coinciden. Favor de validar.");
		   alert("El Password y su confirmacion no coinciden. Favor de validar.");
		}
      }
      </script>
    `;
  }
}

window.customElements.define('my-view1', MyView1);
