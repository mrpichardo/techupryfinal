/* Load the PolymerElement base class and html helper function */
import { PolymerElement, html } from '@polymer/polymer/polymer-element.js';
/* Load shared styles. All view elements use these styles */
import './shared-styles.js';

/* Extend the base PolymerElement class */
class MyAltaMovs extends PolymerElement {
  /* Define a template for the new element */
  static get template() {
    return html`
      <style include="shared-styles">
        :host {
          display: block;

          padding: 10px;
        }
        body {font-family: Arial, Helvetica, sans-serif;}

        /* Full-width input fields */
        input[type=text],     input[type=password],
        input[type=email],    input[type=date],
        input[type=currency], input[type=number] {
          width: 100%;
          padding: 12px 20px;
          margin: 8px 0;
          display: inline-block;
          border: 1px solid #ccc;
          box-sizing: border-box;
        }

        /* Set a style for all buttons */
        button {
          background-color: #4CAF50;
          color: white;
          padding: 14px 20px;
          margin: 8px 0;
          border: none;
          cursor: pointer;
          width: 100%;
        }

        button:hover {
          opacity: 0.8;
        }

        /* Extra styles for the cancel button */
        .cancelbtn {
          width: auto;
          padding: 10px 18px;
          background-color: #f44336;
        }

        /* Center the image and position the close button */
        .imgcontainer {
           text-align: center;
           margin: 24px 0 12px 0;
           position: relative;
        }

        img.avatar {
          width: 40%;
          border-radius: 50%;
        }

        .container {
          padding: 16px;
        }

        span.psw {
          float: right;
          padding-top: 16px;
        }

        /* The Close Button (x) */
        .close {
           position: absolute;
           right: 25px;
           top: 0;
           color: #000;
           font-size: 35px;
           font-weight: bold;
        }

        .close:hover,
        .close:focus {
           color: red;
           cursor: pointer;
        }

        /* Add Zoom Animation */
        .animate {
          -webkit-animation: animatezoom 0.6s;
          animation: animatezoom 0.6s
        }

        @-webkit-keyframes animatezoom {
          from {-webkit-transform: scale(0)}
            to {-webkit-transform: scale(1)}
        }

        @keyframes animatezoom {
          from {transform: scale(0)}
            to {transform: scale(1)}
        }

        /* Change styles for span and cancel button on extra small screens */
        @media screen and (max-width: 300px) {
           span.psw {
             display: block;
             float: none;
           }
           .cancelbtn {
             width: 100%;
           }
        }
      </style>

      <div class="card">
        <div class="circle">AM</div>
        <h1>Alta de Movimientos</h1>
        <p>Da de alta un movimiento nuevo en la BD.</p>
        <div class="container">
           <form name="form" id="form">
              <div class="field">
                 <label for="userid"><b>Usuario</b></label>
                 <input type="text" id="userid" placeholder="ID Usuario" name="userid" required></code>
              </div>
              <div class="field">
                 <label for="nombre"><b>Nombre</b></label>
                 <input type="text" id="nombre" placeholder="Nombre" name="nombre" required></code>
              </div>
              <div class="field">
                 <label for="apellido"><b>Apellido</b></label>
                 <input type="text" id="apellido" placeholder="Apellido" name="apellido" required></code>
              </div>
              <div class="field">
                 <label for="fecha"><b>Fecha</b></label>
                 <input type="date" id="fecha" placeholder="Fecha de movimiento" name="fecha" required></code>
              </div>
              <div class="field">
                 <label for="importe"><b>Importe</b></label>
                 <input type="number" id="importe" placeholder="Importe" name="importe" pattern="\d+(\.\d{2})?" title="Utilice formato tipo 100.00" required></code>
              </div>
              <div class="field">
                 <label for="categoria"><b>Categoria</b></label>
                 <input type="text" id="categoria" placeholder="Categoria (A, B, C, D, E, F)" name="categoria" pattern="[A-F]{1,1}" required></code>
              </div>
              <button onclick="altaMovimiento()">Registrar</button>
              <button type="button" class="cancelbtn">Cancelar</button>
              <span class="psw"><a href="#">Inicio</a></span>
              <br><br>
           </form>
        </div>
      </div>
      <script>

      function altaMovimiento(){
         var url="http://localhost:3000/api/movimientos";
         var request=new XMLHttpRequest();
         request.onreadystatechange=function(){
			 if (this.readyState == 4 && this.status == 200){
				 console.log(JSON.parse(request.status));
				 window.open ('success.htm', '_blank');
		     }else{
				 if (this.readyState == 4){
					 alert("Error en alta de login, favor de validar... Codigo de error: " + this.status + ".");
				     console.log("Error en alta de login, favor de validar... Codigo de error: " + this.status + ".");
                     window.open ('unsuccess.htm', '_blank');
			     }
		     }
	     }
         request.open("POST", url, true);
         request.setRequestHeader("Accept", "application/json");
         request.setRequestHeader("Content-Type", "application/json");
         var movimiento={idcliente: form.userid.value,
                         nombre: form.nombre.value,
                         apellido: form.apellido.value,
                         fecha: form.fecha.value,
                         importe: form.importe.value,
                         categoria: form.categoria.value};
         var body = JSON.stringify(movimiento);
         request.send(body);
      }
      </script>
    `;
  }
}
/* Register the new element with the browser */
window.customElements.define('my-alta-movs', MyAltaMovs);